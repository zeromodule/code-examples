<?php

namespace Application\Core\Components;

use Doctrine\Common\Collections\Collection;
use Application\Core\Components\Questions\Decorators\GroupQuestion;
use Application\Core\Components\Questions\Decorators\RepeatedInstanceQuestion;
use Application\Core\Components\Questions\Decorators\RepeatedQuestion;
use Application\Core\Components\Questions\IQuestion;
use Application\Core\Components\Questions\Question;
use Application\Core\Components\Questions\QuestionHolder;
use Application\Core\Entity\Project;
use Application\Core\Entity\ProjectElement;
use Application\Core\Entity\UserTask;
use Application\Core\Repository\ProjectElementRepository;


class Questionnaire extends QuestionHolder
{
    const TYPE_SINGLE = 1;
    const TYPE_MULTI = 2;
    const TYPE_YESNO = 3;
    const TYPE_FREE = 4;
    const TYPE_PHOTO = 5;
    const TYPE_VIDEO = 6;
    const TYPE_AUDIO = 7;
    const TYPE_NOTE = 8;
    const TYPE_ATTACHMENT = 9;
    const TYPE_HEADER = 10;
    const TYPE_GROUP = 11;
    const TYPE_REPEATED = 12;
    const TYPE_GAP = 13;
    const TYPE_DROPDOWN = 14;

    public static $types = [
        self::TYPE_SINGLE => 'Single',
        self::TYPE_MULTI => 'Multi',
        self::TYPE_YESNO => 'YesNo',
        self::TYPE_FREE => 'Free',
        self::TYPE_PHOTO => 'Photo',
        self::TYPE_VIDEO => 'Video',
        self::TYPE_AUDIO => 'Audio',
        self::TYPE_NOTE => 'Note',
        self::TYPE_ATTACHMENT => 'Attachement',
        self::TYPE_HEADER => 'Header',
        self::TYPE_GROUP => 'Group',
        self::TYPE_REPEATED => 'Repeated',
        self::TYPE_GAP => 'Gap',
        self::TYPE_DROPDOWN => 'Dropdown',
    ];

    /**
     * @var Question[]
     */
    private $directQuestionAddress = [];

    /**
     * @var array
     */
    private $groupQuestions = [];

    /**
     * @param UserTask $userTask
     * @param Project  $project
     */
    public function __construct(Project $project, UserTask $userTask = null)
    {
        /** @var ProjectElementRepository $elementsRepository */
        $elementsRepository = \Kohana::$dic->getEm()->getRepository(ProjectElement::className());

        parent::__construct();

        $this->buildTree(
            $elementsRepository->getByProjectVersionId(
                $userTask->getProjectVersionId()
            )
        );

        if ($userTask !== null){
            $this->addAnswers( $userTask->getAnswers() );
        }
    }

    /**
     * @param UserTask $userTask
     *
     * @return Questionnaire
     */
    public static function buildFromUserTask(UserTask $userTask)
    {
        return new self($userTask->getAddress()->getProject(), $userTask);
    }

    /**
     * @param Project $project
     *
     * @return Questionnaire
     */
    public static function buildFromProject(Project $project)
    {
        return new self($project);
    }

    /**
     * @param IQuestion $question
     */
    public function addQuestion(IQuestion $question)
    {
        if (!isset($this->directQuestionAddress[$question->getId()])){
            $this->directQuestionAddress[$question->getId()] = $question;

            if ($this->isGroup($question)){
                $this->groupQuestions[]=$question;
            }

            if (!$question->inRepeat() && !$this->isGroup($question)){
                parent::addQuestion($question);
            }
        }

        if ($question->hasParents()){
            foreach ($question->getParents() as $parent)
            {
                if ($parentQuestion = $this->getQuestion($parent->getId())){
                    $parentQuestion->addQuestion($question);
                }
            }
        }
    }

    public function getQuestions()
    {
        return $this;
    }

    /**
     * @return IQuestion[]
     */
    public function getGroupQuestions()
    {
        return $this->groupQuestions;
    }

    /**
     * @param IQuestion $question
     * @return bool
     */
    protected function isGroup(IQuestion $question)
    {
        return (int)$question->getType() === self::TYPE_GROUP;
    }

    /**
     * @param IQuestion $question
     * @return bool
     */
    protected function isRepeat(IQuestion $question)
    {
        return (int)$question->getType() === self::TYPE_REPEATED;
    }

    /**
     * @param $id
     *
     * @return null|Question
     */
    protected function getQuestion($id)
    {
        return isset($this->directQuestionAddress[$id])?$this->directQuestionAddress[$id]:null;
    }

    /**
     * @param Collection|ProjectElement[] $elements
     */
    protected function buildTree(Collection $elements)
    {
        $withParent = [];

        foreach ($elements as $element)
        {
            $question = new Question($element);

            if ($this->isRepeat($question)){
                $question = new RepeatedQuestion($question);
            }

            if ($this->isGroup($question)){
                $question = new GroupQuestion($question);
            }

            if ($question->hasParents()){

                if ($question->inRepeat()){
                    $question = new RepeatedInstanceQuestion($question);
                }
                $withParent[]=$question;
            }else{
                $this->addQuestion($question);
            }
        }

        foreach ($withParent as $question)
        {
            $this->addQuestion($question);
        }

    }

    /**
     * @param $answers
     */
    protected function addAnswers($answers)
    {
        /** @var \Application\Core\Entity\UserTaskAnswer $answer */
        foreach ($answers as $answer)
        {
            if ($question = $this->getQuestion($answer->getElement()->getId())){
                $question->addAnswer($answer);
            }
        }

    }
}