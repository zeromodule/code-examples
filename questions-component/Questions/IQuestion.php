<?php
namespace Application\Core\Components\Questions;

use Application\Core\Entity\ProjectElement;
use Application\Core\Entity\ProjectElementValue;

interface IQuestion extends IQuestionHolder, IAnswerAccess
{

    /**
     * Inner question options
     *
     * @param $name
     *
     * @return mixed
     */
    public function getOption($name);

    /**
     * Answer options
     * @return ProjectElementValue[]
     */
    public function getOptions();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return int
     */
    public function getType();

    public function getId();

    public function getUniqueQuestionId();

	public function getPosition();

    public function hasParents();

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|ProjectElement[]
     */
    public function getParents();

    /**
     * @return string
     */
    public function getTypeName();

    /**
     * @return bool
     */
    public function inRepeat();
}