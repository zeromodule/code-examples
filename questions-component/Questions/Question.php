<?php

namespace Application\Core\Components\Questions;

use Application\Core\Components\Questionnaire;
use Application\Core\Entity\ProjectElement;
use Application\Core\Entity\ProjectElementValue;
use Application\Core\Entity\UserTaskAnswer;

class Question extends QuestionHolder implements IQuestion
{
    protected $element;

    protected $answers = [];

    /**
     * @param ProjectElement $element
     */
    public function __construct(ProjectElement $element)
    {
        $this->element = $element;
        parent::__construct();
    }

    /**
     * Inner question options
     *
     * @param $name
     *
     * @return mixed
     */
    public function getOption($name)
    {
        return $this->element->getOption($name);
    }

    /**
     * Answer options
     * @return ProjectElementValue[]
     */
    public function getOptions()
    {
        return $this->element->getElementValues();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->element->getName();
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->element->getType();
    }

    public function getId()
    {
        return $this->element->getId();
    }


	public function getPosition()
	{
		return $this->element->getPosition();
	}

    public function hasParents()
    {
        return (bool)count($this->element->getParents());
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|ProjectElement[]
     */
    public function getParents()
    {
        return $this->element->getParents();
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return Questionnaire::$types[$this->getType()];
    }

    /**
     * @return UserTaskAnswer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param UserTaskAnswer $answer
     *
     * @return mixed
     */
    public function addAnswer(UserTaskAnswer $answer)
    {
        $this->answers[] = $answer;
    }

    /**
     * Returns first answer in the set
     *
     * @return UserTaskAnswer
     */
    public function getAnswer()
    {
        return array_values($this->answers)[0];
    }

    /**
     * @return bool
     */
    public function hasAnswers()
    {
        return (bool)count($this->answers);
    }

    /**
     * @return bool
     */
    public function inRepeat()
    {
        return $this->element->inRepeat();
    }

    public function getUniqueQuestionId() {
        return $this->element->getUniqueQuestionId();
    }
}