<?php
namespace Application\Core\Components\Questions;


interface IQuestionHolder extends \IteratorAggregate
{

    /**
     * @param IQuestion $question
     *
     * @return void
     */
    public function addQuestion(IQuestion $question);

    /**
     * @return bool
     */
    public function hasQuestions();
}