<?php

namespace Application\Core\Components\Questions;

use Traversable;

class QuestionHolder implements IQuestionHolder
{

	protected $elements = [];

	protected $sortedFlag = false;

    public function __construct()    {

    }

    /**
     * @param IQuestion $question
     *
     * @return void
     */
    public function addQuestion(IQuestion $question)
    {
        $this->elements[$question->getPosition()]=$question;
    }

    public function hasQuestions()
    {
        return (bool)count($this->elements);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     *       <b>Traversable</b>
     */
    public function getIterator()
    {
	    $this->sort();
        return new \ArrayIterator($this->elements);
    }

	/**
	 * Sort elements by index, only once
	 */
	protected function sort()
	{
		if (!$this->sortedFlag){
			ksort($this->elements);
			$this->sortedFlag = true;
		}
	}
}