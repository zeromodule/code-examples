<?php
namespace Application\Core\Components\Questions;

use Application\Core\Entity\UserTaskAnswer;

interface IAnswerAccess
{
    /**
     * @return UserTaskAnswer[]
     */
    public function getAnswers();

    /**
     * Returns first answer in the set
     * @return UserTaskAnswer
     */
    public function getAnswer();

    /**
     * @return bool
     */
    public function hasAnswers();

    /**
     * @param UserTaskAnswer $answer
     *
     * @return mixed
     */
    public function addAnswer(UserTaskAnswer $answer);
} 