<?php
namespace Application\Core\Components\Questions\Decorators;

use Application\Core\Components\Questions\IQuestion;
use Application\Core\Entity\ProjectElement;
use Application\Core\Entity\ProjectElementValue;
use Application\Core\Entity\UserTaskAnswer;
use Traversable;

abstract class Question implements IQuestion
{

    /**
     * @var IQuestion
     */
    protected $question;

    public function __construct(IQuestion $question)
    {
        $this->question = $question;
    }

    /**
     * Inner question options
     *
     * @param $name
     *
     * @return mixed
     */
    public function getOption($name)
    {
        return $this->question->getOption($name);
    }

    /**
     * @return ProjectElementValue[]
     */
    public function getOptions()
    {
        return $this->question->getOptions();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->question->getName();
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->question->getType();
    }

    public function getId()
    {
        return $this->question->getId();
    }

	public function getPosition()
	{
		return $this->question->getPosition();
	}

    public function hasParents()
    {
        return $this->question->hasParents();
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|ProjectElement[]
     */
    public function getParents()
    {
        return $this->question->getParents();
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->question->getTypeName();
    }


    /**
     * @return bool
     */
    public function hasQuestions()
    {
        return $this->question->hasQuestions();
    }

    /**
     * @return bool
     */
    public function inRepeat()
    {
        return $this->question->inRepeat();
    }


    /**
     * Add question to holder
     *
     * @param IQuestion $question
     *
     * @return void
     */
    public function addQuestion(IQuestion $question)
    {
        $this->question->addQuestion($question);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     *       <b>Traversable</b>
     */
    public function getIterator()
    {
        return $this->question->getIterator();
    }

    /**
     * @return UserTaskAnswer[]
     */
    public function getAnswers()
    {
        return $this->question->getAnswers();
    }

    /**
     * Returns first answer in the set
     *
     * @return UserTaskAnswer|false
     */
    public function getAnswer()
    {
        return $this->question->getAnswer();
    }

    /**
     * @return bool
     */
    public function hasAnswers()
    {
        return $this->question->hasAnswers();
    }

    /**
     * @param UserTaskAnswer $answer
     *
     * @return mixed
     */
    public function addAnswer(UserTaskAnswer $answer)
    {
        return $this->question->addAnswer($answer);
    }

    public function getUniqueQuestionId() {
        return $this->question->getUniqueQuestionId();
    }
}