<?php
namespace Application\Core\Components\Questions\Decorators;


use Application\Core\Components\Questions\IQuestion;


class RepeatedQuestion extends Question
{

    protected $iterator;

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return RepeatedQuestionsIterator An instance of an object implementing <b>Iterator</b> or
     *       <b>Traversable</b>
     */
    public function getIterator()
    {
        if ($this->iterator === null){
            /** @var RepeatedInstanceQuestion[] $questions */
            $questions = $this->question;
            $this->iterator = new RepeatedQuestionsIterator($questions);
        }
       return $this->iterator;
    }

    /**
     * Add question to holder
     *
     * @param IQuestion $question
     *
     * @return void
     */
    public function addQuestion(IQuestion $question)
    {
        /** @var RepeatedInstanceQuestion $question */
        $question->setOuterIterator($this->getIterator());
        $this->question->addQuestion($question);
    }

}