<?php
namespace Application\Core\Components\Questions\Decorators;


use Application\Core\Components\Questionnaire;
use Application\Core\Components\Questions\IQuestion;

class GroupQuestion extends Question
{

    protected $plan;

    protected $fact;

    /**
     * @return float|int
     */
    public function getPlan()
    {
        if ($this->plan === null){
            $this->plan = $this->buildPlan();
        }

        return $this->plan;
    }

    /**
     * @return float|int
     */
    public function getFact()
    {
        if ($this->fact === null){
            $this->fact = $this->buildFact();
        }

        return $this->buildFact();
    }

    /**
     * This one is needed for excel auto generation
     *
     * [0 => plan]
     * [1 => fact]
     * @return array
     */
    public function getAnswers()
    {
        return [
            $this->getPlan(),
            $this->getFact()
        ];
    }

    /**
     * Returns first answer in the set
     *
     * @return float|int
     */
    public function getAnswer()
    {
        return $this->getPlan();
    }

    public function hasAnswers()
    {
        return true;
    }

    /**
     * We don't need iteration of inner questions
     * @return bool
     */
    public function hasQuestions()
    {
        return false;
    }

    /**
     * We don't need iteration of inner questions
     * @return bool
     */
    public function getIterator()
    {
        return new \ArrayIterator([]);
    }

    /**
     * Only for plan and fact acquire
     * @return \Traversable
     */
    protected function getInnerIterator()
    {
        return parent::getIterator();
    }

    protected function buildPlan()
    {
        $plan = 0;

        /** @var IQuestion $question */
        foreach ($this->getInnerIterator() as $question) {
            if ((int)$question->getType() === Questionnaire::TYPE_SINGLE){
                $options = $question->getOptions();

                $max = 0;
                foreach ($options as $option)
                {
                    $max = max($option->getRate(),$max);
                }
                $plan+=$max;
            }
        }

        return $plan;
    }

    protected function buildFact()
    {
        $fact = 0;

        /** @var IQuestion $question */
        foreach ($this->getInnerIterator() as $question) {
            if ((int)$question->getType() === Questionnaire::TYPE_SINGLE && $question->getAnswer() && $question->getAnswer()->getElementValue()){
                $fact+=$question->getAnswer()->getElementValue()->getRate();
            }
        }

        return $fact;
    }
} 