<?php
namespace Application\Core\Components\Questions\Decorators;


use Application\Core\Entity\UserTaskAnswer;

class RepeatedInstanceQuestion extends Question
{

    /**
     * @var RepeatedQuestionsIterator
     */
    protected $outerIterator;

    protected $answers = [];

    /**
     * Add parent repeater to notify about iterations in answers
     * @param RepeatedQuestionsIterator $outerIterator
     */
    public function setOuterIterator(RepeatedQuestionsIterator $outerIterator)
    {
        $this->outerIterator = $outerIterator;
    }

    /**
     * Returns answers for the current iteration
     *
     * @return UserTaskAnswer[]
     */
    public function getAnswers()
    {
        if (isset($this->answers[$this->outerIterator->getCurrentIteration()])){
            return $this->answers[$this->outerIterator->getCurrentIteration()];
        }

        return [];
    }


    /**
     * Returns first answer in the set
     *
     * @return UserTaskAnswer|false
     */
    public function getAnswer()
    {
        if (isset($this->answers[$this->outerIterator->getCurrentIteration()])){
            return array_values($this->answers[$this->outerIterator->getCurrentIteration()])[0];
        }

        return false;
    }

    /**
     * @param UserTaskAnswer $answer
     *
     * @return mixed
     */
    public function addAnswer(UserTaskAnswer $answer)
    {
        $iteration = (int)$answer->getIteration();

        if (!isset($this->answers[$iteration])){
            $this->answers[$iteration] = [];
            $this->outerIterator->addIteration($iteration);
        }

        $this->answers[$iteration][] = $answer;
    }

    public function hasAnswers()
    {
        if (isset($this->answers[$this->outerIterator->getCurrentIteration()])){
            return (bool)count($this->answers[$this->outerIterator->getCurrentIteration()]);
        }

        return false;
    }
}