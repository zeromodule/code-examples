<?php
namespace Application\Core\Components\Questions\Decorators;


class RepeatedQuestionsIterator implements \Iterator
{


    protected $iterations = [];

    protected $currentIteration;

    /**
     * @var RepeatedInstanceQuestion[]
     */
    protected $questions;

    /**
     * Flag
     * We need at least one set of questions
     * Even if we dont have any iterations
     * @var bool
     */
    protected $blankIteration = true;


    /**
     * @param RepeatedInstanceQuestion[] $questions
     */
    public function __construct($questions)
    {
        $this->questions = $questions;
    }

    public function getCurrentIteration()
    {
        return $this->currentIteration;
    }


    /**
     * @param integer $int
     */
    public function addIteration($int)
    {
        $this->blankIteration = false;
        $this->iterations[$int] = $int;

        if (count($this->iterations)=== 1){
            $this->currentIteration = current($this->iterations);
        }
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     *
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->questions;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     *
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->currentIteration = next($this->iterations);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->currentIteration;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     *       Returns true on success or false on failure.
     */
    public function valid()
    {
        if ($this->currentIteration !== false){
            return true;
        }

        return false;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if ($this->blankIteration){
            $this->currentIteration = 0;
        }else{
            $this->currentIteration = reset($this->iterations);
        }
    }
} 