<?php
namespace application\domain\news\repository;

use application\contracts\QueryParametersInterface;
use application\domain\news\collections\NewsCollection;
use application\domain\news\News;

/**
 * Interface NewsRepositoryInterface
 * @package application\domain\news\repository
 */
interface NewsRepositoryInterface
{
    /**
     * @param NewsCollection $collection
     * @return mixed
     */
    public function createAll(NewsCollection $collection);

    /**
     * @param array $filter
     * @param array $values
     * @return mixed
     */
    public function updateByCondition(array $filter, array $values);

    /**
     * @param News $model
     * @return News
     */
    public function save(News $model): News;

    /**
     * @param News $model
     * @return mixed
     */
    public function delete(News $model);

    /**
     * @param array $filter
     * @return mixed
     */
    public function deleteByCondition(array $filter);

    /**
     * @param QueryParametersInterface $queryParameters
     * @return array
     */
    public function list(QueryParametersInterface $queryParameters): array;

    /**
     * @param QueryParametersInterface $queryParameters
     * @return int
     */
    public function count(QueryParametersInterface $queryParameters): int;

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}