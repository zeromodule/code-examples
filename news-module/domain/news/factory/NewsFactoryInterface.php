<?php
namespace application\domain\news\factory;

use application\domain\news\collections\NewsCollection;
use application\domain\news\entity\NewsInterface;
use application\domain\news\News;

/**
 * Interface NewsFactoryInterface
 * @package application\domain\news\factory
 */
interface NewsFactoryInterface
{
    /**
     * @param NewsInterface $entity
     * @return News
     */
    public function createNewsFromEntity(NewsInterface $entity): News;

    /**
     * @param array $entityArray
     * @return NewsCollection
     */
    public function createCollectionFromEntityArray(array $entityArray): NewsCollection;
}