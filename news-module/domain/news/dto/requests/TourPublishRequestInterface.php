<?php
namespace application\domain\news\dto\requests;

/**
 * Interface TourPublishNewsCreationRequestInterface
 *
 * @package application\domain\news\dto\requests
 */
interface TourPublishRequestInterface extends CreateRequestInterface
{
    public function getTourId();

    public function getTourName();

    public function getPanoramaId();

    public function getTourLocation();

    public function getTourCover();

    public function getTourSnapshotVersion();
}