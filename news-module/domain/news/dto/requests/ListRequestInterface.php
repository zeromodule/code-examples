<?php
namespace application\domain\news\dto\requests;

use application\contracts\QueryParametersInterface;

interface ListRequestInterface extends QueryParametersInterface
{
}