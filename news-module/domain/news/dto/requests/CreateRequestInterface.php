<?php
namespace application\domain\news\dto\requests;

/**
 * Базовый интерфейс для всех запросов, которые создают новости
 * Interface RequestInterface
 * @package application\domain\news\dto\requests
 */
interface CreateRequestInterface extends NewsmakerRequestInterface
{
    public function getReceiversIds(): array;
}