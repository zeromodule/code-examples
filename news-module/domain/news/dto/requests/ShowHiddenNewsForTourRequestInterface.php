<?php
namespace application\domain\news\dto\requests;

interface ShowHiddenNewsForTourRequestInterface
{
    public function getTourId();
}