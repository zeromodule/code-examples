<?php
namespace application\domain\news\dto\requests;

/**
 * Interface TourNewsDeletionRequestInterface
 * @package application\domain\news\dto\requests
 */
interface TourNewsDeletionRequestInterface
{
    public function getTourId();
}