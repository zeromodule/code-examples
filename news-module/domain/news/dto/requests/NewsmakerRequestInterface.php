<?php

namespace application\domain\news\dto\requests;

interface NewsmakerRequestInterface
{
    public function getNewsMakerId();

    public function getNewsMakerUsername();

    public function getNewsMakerName();

    public function getNewsMakerAvatar();
}