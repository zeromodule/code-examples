<?php
namespace application\domain\news\dto\requests;
/**
 * Interface TourCommentNewsCreationRequestInterface
 * @package application\domain\news\dto\requests
 */
interface TourCommentRequestInterface extends CreateRequestInterface
{
    public function getTourId();

    public function getTourName();

    public function getPanoramaId();

    public function getTourLocation();

    public function getTourCover();

    public function getTourSnapshotVersion();

    public function getCommentId();
}