<?php
namespace application\domain\news\dto\requests;
/**
 * Interface TourFavoriteRequestInterface
 * @package application\domain\news\dto\requests
 */
interface TourFavoriteRequestInterface extends CreateRequestInterface
{
    public function getTourId();

    public function getTourName();

    public function getPanoramaId();

    public function getTourLocation();

    public function getTourCover();

    public function getTourSnapshotVersion();
}