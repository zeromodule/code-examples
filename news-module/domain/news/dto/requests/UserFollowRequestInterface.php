<?php
namespace application\domain\news\dto\requests;

/**
 * Interface UserFollowRequestInterface
 * @package application\domain\news\dto\requests
 */
interface UserFollowRequestInterface extends CreateRequestInterface
{
    /**
     * Возвращает того, кого зафоловилли
     */
    public function getFollowedUserId();
}