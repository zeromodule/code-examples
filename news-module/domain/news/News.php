<?php
namespace application\domain\news;

class News
{
    const EVENT_REGISTER = 'register';
    const EVENT_TOUR_PUBLISH = 'tour/publish';
    const EVENT_TOUR_FAVORITE = 'tour/favorite';
    const EVENT_TOUR_COMMENT = 'tour/comment';
    const EVENT_FOLLOW = 'follow';
    const EVENT_TOUR_PANORAMA = 'tour/panorama';

    public static $groups = [
        'spaces_create' => [self::EVENT_TOUR_PUBLISH],
        'spaces_update' => [self::EVENT_TOUR_PANORAMA],
        'spaces_like' => [self::EVENT_TOUR_FAVORITE],
        'spaces_comment' => [self::EVENT_TOUR_COMMENT],
        'users_register' => [self::EVENT_REGISTER],
        'users_follow' => [self::EVENT_FOLLOW],
    ];

    public static $optional_fields = [
        'tour_id', 'tour_panorama_id', 'comment_id', 'is_hidden', 'is_deleted'
    ];

    public static $typeFieldsMap = [
        self::EVENT_REGISTER => [
            'newsmaker_user_id',
        ],
        self::EVENT_TOUR_PUBLISH => [
            'newsmaker_user_id',
            'tour_id',
            'tour_panorama_id',
        ],
        self::EVENT_TOUR_FAVORITE => [
            'newsmaker_user_id',
            'tour_id',
            'tour_panorama_id',
        ],
        self::EVENT_FOLLOW => [
            'newsmaker_user_id',
        ],
        self::EVENT_TOUR_COMMENT => [
            'newsmaker_user_id',
            'tour_id',
            'tour_panorama_id',
            'comment_id'
        ],
        self::EVENT_TOUR_PANORAMA => [
            'newsmaker_user_id',
            'tour_id',
            'tour_panorama_id',
        ]
    ];

    private $newsmaker_user_id;
    private $news_id;
    private $user_id;
    private $comment_id;
    private $create_time;
    private $is_deleted = false;
    private $is_hidden = false;
    private $tour_id;
    private $tour_panorama_id;
    private $type;

    private $tour_params = [
        'snapshot_version' => null,
        'tour_cover'       => null,
        'tour_name'        => null,
        'tour_location'    => null
    ];

    private $newsmaker_params = [
        'newsmaker_avatar'   => null,
        'newsmaker_username' => null,
        'newsmaker_name'     => null,
    ];

    /**
     * News constructor.
     * @param $newsmaker_user_id
     * @param $user_id
     * @param string $type
     */
    public function __construct($newsmaker_user_id, $user_id, string $type)
    {

        if (!isset(self::$typeFieldsMap[$type])) {
            throw new \InvalidArgumentException('Unknown news type ' . $type);
        }

        $this->type = $type;
        $this->newsmaker_user_id = $newsmaker_user_id;
        $this->user_id = $user_id;
    }


    /**
     * @return mixed
     */
    public function getNewsmakerUserId()
    {
        return $this->newsmaker_user_id;
    }

    /**
     * @return mixed
     */
    public function getNewsId()
    {
        return $this->news_id;
    }

    /**
     * @param $news_id
     * @return $this
     */
    public function setNewsId($news_id)
    {
        $this->news_id = $news_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getCommentId()
    {
        return $this->comment_id;
    }

    /**
     * @param $comment_id
     * @return $this
     */
    public function setCommentId($comment_id)
    {
        $this->comment_id = $comment_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }

    /**
     * @param $create_time
     * @return $this
     */
    public function setCreateTime($create_time)
    {
        $this->create_time = $create_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param bool $is_deleted
     * @return $this
     */
    public function setIsDeleted(bool $is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsHidden()
    {
        return $this->is_hidden;
    }

    /**
     * @param bool $is_hidden
     * @return $this
     */
    public function setIsHidden(bool $is_hidden)
    {
        $this->is_hidden = $is_hidden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTourId()
    {
        return $this->tour_id;
    }

    /**
     * @param $tour_id
     * @return $this
     */
    public function setTourId($tour_id)
    {
        $this->tour_id = $tour_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTourPanoramaId()
    {
        return $this->tour_panorama_id;
    }

    /**
     * @param $tour_panorama_id
     * @return $this
     */
    public function setTourPanoramaId($tour_panorama_id)
    {
        $this->tour_panorama_id = $tour_panorama_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTourParams()
    {
        return $this->tour_params;
    }

    /**
     * @param mixed $tour_params
     */
    public function setTourParams($tour_params)
    {
        $this->tour_params = $tour_params;
    }

    /**
     * @return mixed
     */
    public function getNewsmakerParams()
    {
        return $this->newsmaker_params;
    }

    /**
     * @param mixed $newsmaker_params
     */
    public function setNewsmakerParams($newsmaker_params)
    {
        $this->newsmaker_params = $newsmaker_params;
    }
}