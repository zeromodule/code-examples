<?php
namespace application\domain\news\collections;

use application\domain\news\News;

class NewsCollection implements \Countable, \Iterator, \ArrayAccess
{
    /**
     * @var News[]
     */
    private $items = [];

    /**
     * @var int
     */
    private $position = 0;

    /**
     * @param News $item
     * @return $this
     */
    public function add(News $item)
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    public function next()
    {
        ++$this->position;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    /**
     * @return News
     */
    public function current()
    {
        return $this->items[$this->position];
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function offsetExists($offset)
    {
        return isset($this->items[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->items[$offset]) ? $this->items[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function isEmpty()
    {
        return empty($this->items);
    }
}