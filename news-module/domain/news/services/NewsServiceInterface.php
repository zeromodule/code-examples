<?php
namespace application\domain\news\services;

use application\contracts\dto\ListResponseInterface;
use application\contracts\dto\ResponseInterface;
use application\contracts\QueryParametersInterface;
use application\domain\news\dto\requests\TourNewsDeletionRequestInterface;
use application\domain\news\dto\requests\TourPublishRequestInterface;
use application\domain\news\dto\requests\TourUpdateRequestInterface;
use application\domain\news\dto\requests\UserFollowRequestInterface;
use application\domain\news\factory\NewsFactoryInterface;
use application\domain\news\repository\NewsRepositoryInterface;
use application\domain\user\entity\UserInterface;
use application\domain\news\dto\requests\TourCommentRequestInterface;
use application\domain\news\dto\requests\TourFavoriteRequestInterface;
use application\domain\news\dto\requests\ShowHiddenNewsForTourRequestInterface;
use application\domain\news\dto\requests\UserRegisterRequestInterface;

/**
 * Сервис для работы с новостями
 * Interface NewsServiceInterface
 * @package application\domain\news\services
 */
interface NewsServiceInterface
{
    /**
     * NewsServiceInterface constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param NewsFactoryInterface $newsFactory
     */
    public function __construct(NewsRepositoryInterface $newsRepository, NewsFactoryInterface $newsFactory);

    /**
     * @param $id
     * @return ResponseInterface
     */
    public function getOneById($id): ResponseInterface;

    /**
     * @param QueryParametersInterface $request
     * @param UserInterface|null $user
     * @return ListResponseInterface
     */
    public function getList(QueryParametersInterface $request, UserInterface $user = null) : ListResponseInterface;

    /**
     * @param TourCommentRequestInterface $request
     * @return ResponseInterface
     */
    public function createForTourComment(TourCommentRequestInterface $request);

    /**
     * @param TourFavoriteRequestInterface $request
     * @return ResponseInterface
     */
    public function createForTourFavorite(TourFavoriteRequestInterface $request);

    /**
     * @param UserFollowRequestInterface $request
     */
    public function createForUserFollow(UserFollowRequestInterface $request);

    /**
     * @param UserRegisterRequestInterface $request
     */
    public function createForUserRegister(UserRegisterRequestInterface $request);

    /**
     * @param TourPublishRequestInterface $request
     * @return ListResponseInterface
     */
    public function createForTourPublish(TourPublishRequestInterface $request);

    /**
     * @param TourUpdateRequestInterface $request
     * @param bool $hidden
     * @return mixed
     */
    public function createForTourUpdate(TourUpdateRequestInterface $request, $hidden = false);

    /**
     * Отображает скрытые новости про новые панорамы в туре.
     * Такие новости создаются, если панорамы грузили в draft, unlisted или приватный тур.
     * @param ShowHiddenNewsForTourRequestInterface $request
     * @return void
     */
    public function showHiddenForTourUpdates(ShowHiddenNewsForTourRequestInterface $request);

    /**
     * @param TourNewsDeletionRequestInterface $request
     * @return void
     */
    public function deleteForTour(TourNewsDeletionRequestInterface $request);

    /**
     * @param UserFollowRequestInterface $request
     * @return void
     */
    public function deleteForUserFollow(UserFollowRequestInterface $request);

    /**
     * @param TourFavoriteRequestInterface $request
     * @return void
     */
    public function deleteForTourFavorite(TourFavoriteRequestInterface $request);

    /**
     * @param TourPublishRequestInterface $request
     * @return void
     */
    public function deleteForTourPublish(TourPublishRequestInterface $request);

    /**
     * @param TourUpdateRequestInterface $request
     * @return void
     */
    public function deleteForTourUpdate(TourUpdateRequestInterface $request);

    /**
     * @param TourCommentRequestInterface $request
     * @return void
     */
    public function deleteForTourComment(TourCommentRequestInterface $request);
}