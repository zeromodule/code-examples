<?php
namespace application\domain\news\entity;

use application\domain\tour\entity\CommentInterface;
use application\domain\tour\entity\TourInterface;
use application\domain\user\entity\UserInterface;

interface NewsInterface
{
    public function getPrimaryKey();

    public function getNewsmakerId();

    public function getUserId();

    public function getType();

    /**
     * @return TourInterface
     */
    public function getTour();

    /**
     * @return CommentInterface
     */
    public function getComment();

    /**
     * @return UserInterface
     */
    public function getNewsmaker();
}