<?php
namespace application\services\news;

use application\contracts\dto\ListResponseInterface;
use application\contracts\dto\ResponseInterface;
use application\contracts\dto\ErrorResponseInterface;
use application\contracts\QueryParametersInterface;
use application\domain\news\collections\NewsCollection;
use application\domain\news\dto\requests\CreateRequestInterface;
use application\domain\news\dto\requests\ShowHiddenNewsForTourRequestInterface;
use application\domain\news\dto\requests\TourCommentRequestInterface;
use application\domain\news\dto\requests\TourFavoriteRequestInterface;
use application\domain\news\dto\requests\TourNewsDeletionRequestInterface;
use application\domain\news\dto\requests\TourPublishRequestInterface;
use application\domain\news\dto\requests\TourUpdateRequestInterface;
use application\domain\news\dto\requests\UserFollowRequestInterface;
use application\domain\news\dto\requests\UserRegisterRequestInterface;
use application\domain\news\factory\NewsFactoryInterface;
use application\domain\news\News;
use application\domain\news\repository\NewsRepositoryInterface;
use application\domain\user\entity\UserInterface;
use application\domain\news\services\NewsServiceInterface;
use application\services\news\dto\requests\ListRequest;
use application\services\news\factory\NewsResponseFactory;

/**
 * Class NewsService
 * @package application\services\news
 */
class NewsService implements NewsServiceInterface
{
    /**
     * @var NewsRepositoryInterface
     */
    private $repository;

    /**
     * @var NewsFactoryInterface
     */
    private $factory;

    /**
     * NewsService constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param NewsFactoryInterface $newsFactory
     */
    public function __construct(NewsRepositoryInterface $newsRepository, NewsFactoryInterface $newsFactory)
    {
        $this->repository = $newsRepository;
        $this->factory = $newsFactory;
    }

    /**
     * @param QueryParametersInterface $request
     * @throws \RuntimeException
     * @return ListResponseInterface
     */
    public function getList(QueryParametersInterface $request, UserInterface $user = null) : ListResponseInterface
    {
        $params = [
            'limit' => $request->getLimit(),
            'start' => $request->getStart(),
            'sort'  => $request->getSort(),
            'filter' => $this->prepareFilter($request)
        ];

        if ($user) {
            $params['filter']['user_id'] = $user->getPrimaryKey();
        }

        $repositoryQuery = new ListRequest($params);

        $result = $this->repository->list($repositoryQuery);
        $totalCount = $this->repository->count($repositoryQuery);
        $collection = $this->factory->createCollectionFromEntityArray($result);
        return NewsResponseFactory::createFromCollection($collection, $totalCount);
    }

    /**
     * @param string $id
     * @return ResponseInterface|ErrorResponseInterface
     */
    public function getOneById($id): ResponseInterface
    {
        return NewsResponseFactory::createFromModel(
            $this->repository->getById($id)
        );
    }

    /**
     * @param TourCommentRequestInterface $request
     * @throws \ConsistencyError
     * @return void
     */
    public function createForTourComment(TourCommentRequestInterface $request)
    {
        $this->createNews(
            News::EVENT_TOUR_COMMENT,
            array_merge(
                [
                    'tour_id'          => $request->getTourId(),
                    'tour_panorama_id' => $request->getPanoramaId(),
                    'comment_id'       => $request->getCommentId()

                ], $this->getNewsMakerData($request)
            ),
            $request->getReceiversIds()
        );
    }

    /**
     * @param TourFavoriteRequestInterface $request
     * @throws \ConsistencyError
     * @return void
     */
    public function createForTourFavorite(TourFavoriteRequestInterface $request)
    {
        $this->createNews(
            News::EVENT_TOUR_FAVORITE,
            array_merge(
                [
                    'tour_id'          => $request->getTourId(),
                    'tour_panorama_id' => $request->getPanoramaId(),

                ], $this->getNewsMakerData($request)
            ),
            $request->getReceiversIds()
        );
    }

    /**
     * @param TourUpdateRequestInterface $request
     * @param bool $hidden
     * @throws \ConsistencyError
     * @return void
     */
    public function createForTourUpdate(TourUpdateRequestInterface $request, $hidden = false)
    {
        $this->createNews(
            News::EVENT_TOUR_PANORAMA,
            array_merge(
                [
                    'tour_id'          => $request->getTourId(),
                    'tour_panorama_id' => $request->getPanoramaId(),
                    'is_hidden'        => $hidden

                ], $this->getNewsMakerData($request)
            ),
            $request->getReceiversIds()
        );
    }

    /**
     * @param ShowHiddenNewsForTourRequestInterface $request
     * @return void
     */
    public function showHiddenForTourUpdates(ShowHiddenNewsForTourRequestInterface $request)
    {
        $this->updateNews(
            [
                'is_deleted' => false,
                'is_hidden'  => true,
                'tour_id'    => $request->getTourId(),
                'type'       => News::EVENT_TOUR_PANORAMA
            ],
            [
                'is_hidden' => false
            ]
        );
    }

    /**
     * @param TourFavoriteRequestInterface $request
     * @return void
     */
    public function deleteForTourFavorite(TourFavoriteRequestInterface $request)
    {
        $this->deleteNews([
            'type'              => News::EVENT_TOUR_FAVORITE,
            'newsmaker_user_id' => $request->getNewsMakerId(),
            'tour_id'           => $request->getTourId()
        ]);
    }

    /**
     * @param UserFollowRequestInterface $request
     * @throws \ConsistencyError
     * @return void
     */
    public function createForUserFollow(UserFollowRequestInterface $request)
    {
        $this->createNews(
            News::EVENT_FOLLOW,
            $this->getNewsMakerData($request),
            $request->getReceiversIds()
        );
    }

    /**
     * @param UserRegisterRequestInterface $request
     * @throws \ConsistencyError
     * @return void
     */
    public function createForUserRegister(UserRegisterRequestInterface $request)
    {
        $this->createNews(
            News::EVENT_REGISTER,
            $this->getNewsMakerData($request),
            $request->getReceiversIds()
        );
    }

    /**
     * @param UserFollowRequestInterface $request
     * @return void
     */
    public function deleteForUserFollow(UserFollowRequestInterface $request)
    {
        $conditions = [
            'type'              => News::EVENT_FOLLOW,
            'user_id'           => $request->getFollowedUserId(),
            'newsmaker_user_id' => $request->getNewsMakerId()
        ];

        $this->deleteNews($conditions);
    }

    /**
     * @param TourPublishRequestInterface $request
     * @throws \ConsistencyError
     * @return void
     */
    public function createForTourPublish(TourPublishRequestInterface $request)
    {
        $this->createNews(
            News::EVENT_TOUR_PUBLISH,
            array_merge(
                [
                    'tour_id'          => $request->getTourId(),
                    'tour_panorama_id' => $request->getPanoramaId(),

                ], $this->getNewsMakerData($request)
            ),
            $request->getReceiversIds()
        );
    }

    /**
     * @param TourPublishRequestInterface $request
     * @return void
     */
    public function deleteForTourPublish(TourPublishRequestInterface $request)
    {
        $this->deleteNews([
            'type'    => News::EVENT_TOUR_PUBLISH,
            'tour_id' => $request->getTourId()
        ]);
    }

    /**
     * @param TourUpdateRequestInterface $request
     * @return void
     */
    public function deleteForTourUpdate(TourUpdateRequestInterface $request)
    {
        $this->deleteNews([
            'type' => News::EVENT_TOUR_PANORAMA,
            'tour_panorama_id' => $request->getPanoramaId()
        ]);
    }

    /**
     * @param TourNewsDeletionRequestInterface $request
     * @return void
     */
    public function deleteForTour(TourNewsDeletionRequestInterface $request)
    {
        $this->deleteNews([
            'tour_id' => $request->getTourId()
        ]);
    }

    /**
     * @param TourCommentRequestInterface $request
     * @return void
     */
    public function deleteForTourComment(TourCommentRequestInterface $request)
    {
        $this->deleteNews([
            'comment_id' => $request->getCommentId()
        ]);
    }

    /**
     * @param string $type
     * @param array $data
     * @param $receivers
     * @return void
     * @throws \ConsistencyError
     */
    private function createNews(string $type, array $data, $receivers)
    {
        $receivers = (array) $receivers;

        if (empty($receivers)) {
            return;
        }

        if (!$this->validateNews($type, $data)) {
            throw new \ConsistencyError('Not valid news type');
        }

        $data['user_id'] = $receivers;
        $data['type'] = $type;

        array_walk_recursive($data, function (&$value, $key) {
            if (is_string($value)) {
                $value = preg_replace("/'/", '`', $value);
            }
        });

        $collection = new NewsCollection();

        foreach ($data['user_id'] as $user_id) {
            $model = new News($data['newsmaker_user_id'], $user_id, $type);

            foreach (News::$optional_fields as $optional_field) {
                if (isset($data[$optional_field])) {
                    $model->{'set' . implode('', array_map('ucfirst', explode('_', $optional_field)))}($data[$optional_field]);
                }
            }

            $collection->add($model);
        }

        $this->repository->createAll($collection);
    }

    /**
     * @param array $conditions
     * @return void
     */
    private function deleteNews(array $conditions)
    {
        $this->repository->deleteByCondition($conditions);
    }

    /**
     * @param array $conditions
     * @param array $values
     * @return void
     */
    private function updateNews(array $conditions, array $values)
    {
        $this->repository->updateByCondition($conditions, $values);
    }

    /**
     * @param string $type
     * @param array $data
     * @return bool
     */
    private function validateNews(string $type, array $data): bool
    {

        if (!array_key_exists($type, News::$typeFieldsMap)) {
            return false;
        }

        if (array_diff(News::$typeFieldsMap[$type], array_keys($data))) {
            return false;
        }
        return true;
    }

    /**
     * @param CreateRequestInterface $request
     * @return array
     */
    private function getNewsMakerData(CreateRequestInterface $request)
    {
        return [
            'newsmaker_user_id'  => $request->getNewsMakerId(),
        ];
    }

    /**
     * @param QueryParametersInterface $request
     * @return array
     */
    private function prepareFilter(QueryParametersInterface $request)
    {
        $filter = $request->getFilter();

        if (!empty($filter['type'])) {
            $groups = explode(',', $filter['type']);
            $in = [];
            foreach($groups as $group) {
                if(!isset(News::$groups[$group])) {
                    continue;
                }
                $in = array_merge($in, News::$groups[$group]);
            }

            $filter['type'] = $in;
        }

        return $filter;
    }
}