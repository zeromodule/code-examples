<?php
namespace application\services\news\factory;

use application\domain\news\collections\NewsCollection;
use application\domain\news\News;
use application\services\news\dto\responses\NewsErrorResponse;
use application\services\news\dto\responses\NewsListResponse;
use application\services\news\dto\responses\NewsResponse;

/**
 * Class NewsResponseFactory
 * @package application\services\news\factory
 */
class NewsResponseFactory
{
    /**
     * @param string $json
     * @return NewsListResponse|NewsResponse|NewsErrorResponse
     */
    static public function createFromJson(string $json)
    {
        $raw = \json_decode($json);

        if (isset($raw->error)) {
            return new NewsErrorResponse($raw->error);
        }

        if ( isset($raw->result->items, $raw->result->totalCount) ) {
            return new NewsListResponse($raw->result->items, $raw->result->totalCount);
        } else {
            return new NewsResponse($raw->result);
        }
    }

    /**
     * @param NewsCollection $collection
     * @param int $totalCount
     * @return NewsListResponse
     */
    static public function createFromCollection(NewsCollection $collection, int $totalCount)
    {
        $items = [];

        /** @var News $item */
        foreach ($collection as $model) {
            $items[] = self::createItemFromModel($model);
        }

        return new NewsListResponse($items, $totalCount);
    }

    /**
     * @param News $model
     * @return NewsResponse
     */
    static public function createFromModel(News $model)
    {
        return new NewsResponse(self::createItemFromModel($model));
    }

    /**
     * @param News $model
     * @return \stdClass
     */
    private static function createItemFromModel(News $model): \stdClass
    {
        $item = [
            'news_id'           => $model->getNewsId(),
            'user_id'           => $model->getUserId(),
            'type'              => $model->getType(),
            'create_time'       => $model->getCreateTime(),
            'newsmaker_user_id' => $model->getNewsmakerUserId(),
            'tour_id'           => $model->getTourId(),
            'tour_panorama_id'  => $model->getTourPanoramaId(),
            'comment_id'        => $model->getCommentId(),
            'is_deleted'        => $model->getIsDeleted(),
            'is_hidden'         => $model->getIsHidden()
        ];

        return (object)array_merge($item, $model->getNewsmakerParams(), $model->getTourParams());
    }
}