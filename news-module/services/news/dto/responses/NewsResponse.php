<?php
namespace application\services\news\dto\responses;
use application\contracts\dto\ResponseInterface;

/**
 * Class NewsResponse
 * @package application\services\news
 */
class NewsResponse implements ResponseInterface
{
    /**
     * @var
     */
    private $data;

    /**
     * NewsResponse constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}