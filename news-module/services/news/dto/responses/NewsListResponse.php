<?php
namespace application\services\news\dto\responses;

use application\contracts\dto\ListResponseInterface;

/**
 * Class NewsListResponse
 * @package application\services\news
 */
class NewsListResponse implements ListResponseInterface
{
    /**
     * @var array
     */
    private $items;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * NewsListResponse constructor.
     * @param array $items
     * @param int $totalCount
     */
    public function __construct(array $items, int $totalCount)
    {
        $this->items = $items;
        $this->totalCount = $totalCount;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}