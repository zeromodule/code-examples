<?php
namespace application\services\news\dto\responses;

use application\contracts\dto\ErrorResponseInterface;

class NewsErrorResponse implements ErrorResponseInterface
{
    private $error;

    public function __construct($error)
    {
        $this->error = $error;
    }

    public function getError()
    {
        return $this->error;
    }
}