<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\TourUpdateRequestInterface;
use application\domain\user\entity\UserInterface;
use application\domain\tour\entity\PanoramaInterface;

/**
 * Class TourUpdateRequest
 * @package application\services\news\dto\requests
 */
class TourUpdateRequest extends AbstractNewsCreationRequest implements TourUpdateRequestInterface
{
    protected $panorama;

    /**
     * TourUpdateRequest constructor.
     * @param PanoramaInterface $panorama
     */
    public function __construct(PanoramaInterface $panorama)
    {
        $this->panorama = $panorama;
        $newsMaker = $this->panorama->getTour()->getUserEntity();
        parent::__construct($newsMaker, $newsMaker->getFollowersIds());
    }

    public function getTourCover()
    {
        return $this->panorama->getTour()->getCover();
    }

    public function getPanoramaId()
    {
        return $this->panorama->getPrimaryKey();
    }

    public function getTourLocation()
    {
        return $this->panorama->getTour()->getLocation();
    }

    public function getTourName()
    {
        return $this->panorama->getTour()->getName();
    }

    public function getTourId()
    {
        return $this->panorama->getTour()->getPrimaryKey();
    }

    public function getTourSnapshotVersion()
    {
        return $this->panorama->getTour()->getSnapshotVersion();
    }
}