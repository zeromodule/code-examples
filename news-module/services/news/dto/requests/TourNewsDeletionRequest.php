<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\TourNewsDeletionRequestInterface;
use application\domain\tour\entity\TourInterface;

/**
 * Class TourNewsDeletionRequest
 * @package application\services\news\dto\requests
 */
class TourNewsDeletionRequest implements TourNewsDeletionRequestInterface
{
    private $tour;

    /**
     * TourNewsDeletionRequest constructor.
     * @param TourInterface $tour
     */
    public function __construct(TourInterface $tour)
    {
        $this->tour = $tour;
    }

    /**
     * @return mixed
     */
    public function getTourId()
    {
        return $this->tour->getPrimaryKey();
    }
}