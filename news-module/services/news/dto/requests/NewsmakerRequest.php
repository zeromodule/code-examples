<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\NewsmakerRequestInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class NewsmakerRequest
 * @package application\services\news\dto\requests
 */
class NewsmakerRequest implements NewsmakerRequestInterface
{
    /**
     * @var UserInterface
     */
    private $newsmaker;

    /**
     * NewsmakerRequest constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->newsmaker = $user;
    }

    public function getNewsMakerId()
    {
        return $this->newsmaker->getPrimaryKey();
    }

    public function getNewsMakerAvatar()
    {
        return $this->newsmaker->getAvatar();
    }

    public function getNewsMakerName()
    {
        return $this->newsmaker->getName();
    }

    public function getNewsMakerUsername()
    {
        return $this->newsmaker->getUsername();
    }
}