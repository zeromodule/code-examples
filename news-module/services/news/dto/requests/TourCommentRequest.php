<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\TourCommentRequestInterface;
use application\domain\tour\entity\CommentInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class TourCommentRequest
 * @package application\services\news\dto\requests
 */
class TourCommentRequest extends AbstractNewsCreationRequest implements TourCommentRequestInterface
{
    /**
     * @var CommentInterface
     */
    protected $comment;

    /**
     * CommentNewsCreationRequest constructor.
     * @param UserInterface $newsMaker
     * @param CommentInterface $comment
     */
    public function __construct(UserInterface $newsMaker, CommentInterface $comment)
    {
        $this->comment = $comment;

        $receiversIds = [
            $this->comment->getTour()->getUserEntity()->getPrimaryKey()
        ];

        parent::__construct($newsMaker, $receiversIds);
    }

    public function getCommentId()
    {
        return $this->comment->getPrimaryKey();
    }

    public function getPanoramaId()
    {
        return $this->comment->getTour()->getPrimaryPanoramaPk();
    }

    public function getTourLocation()
    {
        return $this->comment->getTour()->getLocation();
    }

    public function getTourCover()
    {
        return $this->comment->getTour()->getCover();
    }

    public function getTourSnapshotVersion()
    {
        return $this->comment->getTour()->getSnapshotVersion();
    }

    public function getTourName()
    {
        return $this->comment->getTour()->getName();
    }

    public function getTourId()
    {
        return $this->comment->getTourId();
    }
}