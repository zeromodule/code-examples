<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\TourFavoriteRequestInterface;
use application\domain\tour\entity\TourInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class TourFavoriteRequest
 * @package application\services\news\dto\requests
 */
class TourFavoriteRequest extends AbstractNewsCreationRequest implements TourFavoriteRequestInterface
{
    /**
     * @var TourInterface
     */
    private $tour;

    /**
     * TourFavoriteRequest constructor.
     * @param UserInterface $newsMaker
     * @param TourInterface $tour
     */
    public function __construct(UserInterface $newsMaker, TourInterface $tour)
    {
        $this->tour = $tour;

        $receiversIds = [
            $this->tour->getUserId()
        ];

        parent::__construct($newsMaker, $receiversIds);
    }

    public function getTourCover()
    {
        return $this->tour->getCover();
    }

    public function getPanoramaId()
    {
        return $this->tour->getPrimaryPanoramaPk();
    }

    public function getTourLocation()
    {
        return $this->tour->getLocation();
    }

    public function getTourName()
    {
        return $this->tour->getName();
    }

    public function getTourId()
    {
        return $this->tour->getPrimaryKey();
    }

    public function getTourSnapshotVersion()
    {
        return $this->tour->getSnapshotVersion();
    }
}