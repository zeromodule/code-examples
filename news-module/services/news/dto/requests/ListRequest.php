<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\ListRequestInterface;

/**
 * Class NewsListRequest
 * @package application\services\news\dto\requests
 */
class ListRequest implements ListRequestInterface
{
    private $filter;
    private $limit;
    private $sort;
    private $start;
    private $with;

    /**
     * ListRequest constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        if (isset($params['filter'])) {
            $this->filter = $params['filter'];
        }

        if (isset($params['limit'])) {
            $this->limit = $params['limit'];
        }

        if (isset($params['start'])) {
            $this->start = $params['start'];
        }

        if (isset($params['sort'])) {
            $this->sort = $params['sort'];
        }

        if (isset($params['with'])) {
            $this->with = $params['with'];
        }
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getWith()
    {
        return $this->with;
    }
}