<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\CreateRequestInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class AbstractNewsCreationRequest
 * @package application\services\news\dto\requests
 */
abstract class AbstractNewsCreationRequest implements CreateRequestInterface
{
    private $newsMaker;
    private $receiversIds = [];

    /**
     * AbstractNewsCreationRequest constructor.
     * @param UserInterface $newsMaker
     * @param array $receiversIds
     */
    public function __construct(UserInterface $newsMaker, array $receiversIds)
    {
        $this->newsMaker = $newsMaker;
        $this->receiversIds = $receiversIds;
    }

    public function getNewsMakerId()
    {
        return $this->newsMaker->getPrimaryKey();
    }

    public function getNewsMakerName()
    {
        return $this->newsMaker->getName();
    }

    public function getNewsMakerAvatar()
    {
        return $this->newsMaker->getAvatar();
    }

    public function getNewsMakerUsername()
    {
        return $this->newsMaker->getUsername();
    }

    public function getReceiversIds(): array
    {
        return $this->receiversIds;
    }
}