<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\TourPublishRequestInterface;
use application\domain\tour\entity\TourInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class TourPublishNewsCreationRequest
 * @package application\services\news\dto\requests
 */
class TourPublishRequest extends AbstractNewsCreationRequest implements TourPublishRequestInterface
{
    /**
     * @var TourInterface
     */
    private $tour;

    /**
     * TourPublishNewsCreationRequest constructor.
     * @param TourInterface $tour
     */
    public function __construct(TourInterface $tour)
    {
        $this->tour = $tour;
        $newsMaker = $this->tour->getUserEntity();

        parent::__construct($newsMaker, $newsMaker->getFollowersIds());
    }

    public function getTourCover()
    {
        return $this->tour->getCover();
    }

    public function getPanoramaId()
    {
        return $this->tour->getPrimaryPanoramaPk();
    }

    public function getTourLocation()
    {
        return $this->tour->getLocation();
    }

    public function getTourName()
    {
        return $this->tour->getName();
    }

    public function getTourId()
    {
        return $this->tour->getPrimaryKey();
    }

    public function getTourSnapshotVersion()
    {
        return $this->tour->getSnapshotVersion();
    }
}