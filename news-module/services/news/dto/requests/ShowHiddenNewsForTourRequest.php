<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\ShowHiddenNewsForTourRequestInterface;
use application\domain\tour\entity\TourInterface;

/**
 * Class ShowHiddenNewsForTourRequest
 * @package application\services\news\dto\requests
 */
class ShowHiddenNewsForTourRequest implements ShowHiddenNewsForTourRequestInterface
{
    /**
     * @var TourInterface
     */
    private $tour;

    /**
     * ShowHiddenNewsForTourRequest constructor.
     * @param TourInterface $tour
     */
    public function __construct(TourInterface $tour)
    {
        $this->tour = $tour;
    }

    public function getTourId()
    {
        return $this->tour->getPrimaryKey();
    }
}