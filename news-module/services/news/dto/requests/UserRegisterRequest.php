<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\UserRegisterRequestInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class UserRegisterRequest
 * @package application\services\news\dto\requests
 */
class UserRegisterRequest extends AbstractNewsCreationRequest implements UserRegisterRequestInterface
{
    /**
     * @var UserInterface
     */
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
        parent::__construct($this->user, [$this->user->getPrimaryKey()]);
    }
}