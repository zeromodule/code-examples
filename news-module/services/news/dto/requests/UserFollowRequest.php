<?php
namespace application\services\news\dto\requests;

use application\domain\news\dto\requests\UserFollowRequestInterface;
use application\domain\user\entity\UserInterface;

/**
 * Class UserFollowRequest
 * @package application\services\news\dto\requests
 */
class UserFollowRequest extends AbstractNewsCreationRequest implements UserFollowRequestInterface
{
    /**
     * Тот, кого зафоловили
     * @var UserInterface
     */
    private $followedUser;

    /**
     * UserFollowRequest constructor.
     * @param UserInterface $newsMaker
     * @param UserInterface $followedUser
     */
    public function __construct(UserInterface $newsMaker, UserInterface $followedUser)
    {
        $this->followedUser = $followedUser;

        $receiversIds = [
            $this->getFollowedUserId()
        ];

        parent::__construct($newsMaker, $receiversIds);
    }

    /**
     * @return UserInterface
     */
    public function getFollowedUserId()
    {
        return $this->followedUser->getPrimaryKey();
    }
}